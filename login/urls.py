from django.urls import path
from . import views

app_name = "login"

urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('login/', views.login, name='login'),
    path('register/administrator/', views.administrator_form, name='form-administrator'),
    path('register/dokter/', views.dokter_form, name='form-dokter'),
    path('register/pasien/', views.pasien_form, name='form-pasien'),
]
