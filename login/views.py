from django.shortcuts import render

def welcome(request) :
    return render(request, 'welcome.html')

def login(request) :
    return render(request, 'login.html')

def administrator_form(request) :
    return render(request, 'form/administrator.html')

def dokter_form(request) :
    return render(request, 'form/dokter.html')

def pasien_form(request) :
    return render(request, 'formpasien.html')