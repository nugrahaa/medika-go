from django.urls import path
from . import views

app_name = "administrator"

urlpatterns = [
    path('', views.regis_dokter_rs, name='regis_dokter_rs'),
    path('create-poli/', views.buat_layanan_poliklinik, name='buat_layanan_poliklinik'),
    path('rs-dokter/', views.rs_dokter, name='rs-dokter'),
]
