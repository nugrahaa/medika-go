from django.shortcuts import render

def regis_dokter_rs(request) :
    return render(request, 'regis_dokter_rs.html')

def buat_layanan_poliklinik(request) :
    return render(request, 'buat_layanan_poliklinik.html')

def rs_dokter(request) :
    return render(request, 'rs_dokter.html')